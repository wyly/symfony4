<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CotationRepository")
 */
class Cotation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $cot;




    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCot(): ?string
    {
        return $this->cot;
    }

    public function setCot(string $cot): self
    {
        $this->cot = $cot;

        return $this;
    }

    public function __toString()
    {
        return $this->cot;
    }
}
