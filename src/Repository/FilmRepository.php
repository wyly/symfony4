<?php

namespace App\Repository;

use App\Entity\Film;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;


/**
 * @method Film|null find($id, $lockMode = null, $lockVersion = null)
 * @method Film|null findOneBy(array $criteria, array $orderBy = null)
 * @method Film[]    findAll()
 * @method Film[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method getRequest()
 */
class FilmRepository extends ServiceEntityRepository
{


    public function __construct(ManagerRegistry $registry )
    {
        parent::__construct($registry, Film::class);
    }

    /*Envoie les données pour afficher 6 films sur la page d'accueil les plus récents*/
    /**
     * @return array
     */
    public function findCurrentMovies(): array
    {
        return $this->createQueryBuilder('f')
            ->orderBy('f.id', 'DESC')
            ->setMaxResults(6)
            ->getQuery()
            ->getResult()
            ;
    }

    /*Envoie les données pour afficher un film sur la page categorie*/
    /**
     * @return array
     */
    public function findGenreMovies()
    {
        return $this->createQueryBuilder('f')
            ->orderBy('f.id', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }


    /*Affiche la liste des films pars ajout sur la page cotation*/
    /**
     * @return array
     */
    public function findCotationMovies(): array
    {
        return  $this->createQueryBuilder('f')
            ->orderBy('f.id', 'DESC')
            ->getQuery()
            ->getResult();
    }


    /*Trouve tous les films en fonction de leurs catégories*/
    public function findOneByIdJoinedToCategory($id): array
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT f
            FROM App\Entity\Film f
            JOIN f.categorie c
            WHERE c.id = :id
            ORDER BY f.id DESC'
        )->setParameter('id', $id);

        // returns an array of Product objects
        return $query->getResult();


    }

    /*Trouve tous les films en fonction de leurs cotations*/
    public function findOneByIdJoinedToCotation($id): array
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT f
            FROM App\Entity\Film f
            JOIN f.cotation c
            WHERE c.id = :id'
        )->setParameter('id', $id);

        // returns an array of Product objects
        return $query->getResult();


    }

    /*Trouve tous les films en fonction de la recherche*/
    public function findByTitle($motcle)
    {
        $query = $this->createQueryBuilder('f')
            ->Where('f.titre like :titre')
            ->setParameter('titre', '%'.$motcle.'%')
            ->orderBy('f.titre', 'ASC')
            ->getQuery()
            ;

        // returns an array of Product objects
        return $query->getResult();


    }


    // /**
    //  * @return Film[] Returns an array of Film objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Film
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
