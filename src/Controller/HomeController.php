<?php
/**
 * Created by PhpStorm.
 * User: Nathan
 * Date: 01-12-19
 * Time: 17:54
 */

namespace App\Controller;


use App\Entity\Commentaire;
use App\Entity\Film;
use App\Form\CommentaireType;
use App\Form\FilmType;
use App\Repository\CotationRepository;
use App\Repository\CategorieRepository;
use App\Repository\FilmRepository;
use App\Repository\CommentaireRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use \Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;


class HomeController extends AbstractController
{



    /*Affiche les films récents et les commentaires récents sur la page d'accueil*/
    /**
     * @route ("/", name="home")
     * @param CommentaireRepository $repository
     * @param FilmRepository $filmRepository
     * @return Response
     */
    public function indexcom(CommentaireRepository $repository, FilmRepository $filmRepository): Response
    {
        $films =$filmRepository->findCurrentMovies();
        $commentaires = $repository->findCurrentCommentaire();
        return $this->render('pages/home.html.twig' , [
            'commentaires' =>$commentaires,
            'films' =>$films
        ]) ;
    }


    /*Affiche les détails d'un film sélectionné pars rapport à son ID*/
    /**
     * @Route("/single/{id<\d+>}", name="single")
     * @param int $id
     * @param FilmRepository $repository
     * @param CategorieRepository $categoriesRepository
     * @param CommentaireRepository $commentairesRepository
     * @param Request $request
     * @param ObjectManager $manager
     * @return Response
     */
    public function single(int $id, FilmRepository $repository, CategorieRepository $categoriesRepository, CommentaireRepository $commentairesRepository, Request $request, EntityManagerInterface $manager): Response
    {
        $films = $repository->find($id);
        $commentairesform = new Commentaire();
        $form = $this->createForm(CommentaireType::class, $commentairesform);
        $form->handleRequest($request);


        $users = $this->getUser();

        if ($form->isSubmitted() && $form->isValid()){
            $commentairesform->setFilm($films);
            $commentairesform->setUser($users);
            $manager->persist($commentairesform);
            $manager->flush();

            return $this->redirectToRoute('single',['id' => $films->getId()]);
        }
        $categories = $categoriesRepository->find($id);
        $commentaires = $commentairesRepository->findOneByIdJoinedToFilm($id);
        return $this->render('pages/single.html.twig', [
            'films' => $films,
            'categories' => $categories,
            'commentaires' => $commentaires,
            'commentForm' => $form->createView()
        ]);
    }

    /*Affiche tout les films*/
    /**
     * @route ("/genre}", name="genre")
     * @param CategorieRepository $categorieRepository
     * @param FilmRepository $filmRepository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function action(CategorieRepository $categorieRepository, FilmRepository $filmRepository, PaginatorInterface $paginator, Request $request, int $id = 1): Response
    {
        $categories = $categorieRepository->findAll();
        $filmsCategories = $categorieRepository->find($id);
        $films  =$paginator->paginate(
            $films= $filmRepository->findGenreMovies(),
            $request->query->getInt('page',1),
            18
        );

        return $this->render('pages/genre.html.twig' ,[
            'categories' => $categories,
            'films' => $films,
            'filmsCategories' => $filmsCategories
        ]);
    }

    /*Affiche les films en fonction da la catégorie selectionner*/
    /**
     * @route ("/categorie/{id<\d+>}", name="categorie")
     * @param CategorieRepository $categorieRepository
     * @param FilmRepository $filmRepository
     * @param $id
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function categorie(CategorieRepository $categorieRepository, FilmRepository $filmRepository, $id, PaginatorInterface $paginator, Request $request): Response
    {
        $categories = $categorieRepository->findAll();
        $filmsCategories = $categorieRepository->find($id);
        $films  =$paginator->paginate(
            $films= $filmRepository->findOneByIdJoinedToCategory($id),
            $request->query->getInt('page',1),
            18
        );
        return $this->render('pages/genre.html.twig' ,[
            'categories' => $categories,
            'films' => $films,
            'filmsCategories' => $filmsCategories
        ]);
    }



    /*Affiche le profile de l'utilisateur, toutes ces cordonnées et les commentaires DESC*/
    /**
     * @Route("/profile/{id<\d+>}", name="profile")
     * @param UserRepository $repository
     * @param int $id
     * @param CommentaireRepository $commentairesRepository
     * @return Response
     */
    public function profile(UserRepository $repository, int $id, CommentaireRepository $commentairesRepository): Response
    {
        $users = $repository->find($id);
        $commentaires = $commentairesRepository->findOneByIdJoinedToUser($id);
        return $this->render('pages/profile.html.twig',[
            'users' => $users,
            'commentaires' => $commentaires
        ]);
    }




    /*Affiche la liste des films avec toutes les cotations le $id=0 permet d'afficher toute les cotations quand on on revient d'une*/
    /**
     * @Route("/list", name="list")
     * @param FilmRepository $repository
     * @param CotationRepository $cotationsRepository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function list(FilmRepository $repository, CotationRepository $cotationsRepository, PaginatorInterface $paginator, Request $request, $id = 0): Response
    {
        $films  =$paginator->paginate(
            $films= $repository->findCotationMovies(),
            $request->query->getInt('page',1),
            12
        );
        $filmsCotations = $cotationsRepository->find($id);
        $cotations = $cotationsRepository->findAll();
        return $this->render('pages/list.html.twig', [
            'films' => $films,
            'cotations' => $cotations,
            'filmsCotations' => $filmsCotations
        ]);
    }


    /*Affiche la liste des films en fonction de leurs cotations (id)*/
    /**
     * @Route("/listcotation/{id<\d+>}", name="listcotation")
     * @param FilmRepository $repository
     * @param CotationRepository $cotationsRepository
     * @param int $id
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function listcotation(FilmRepository $repository, CotationRepository $cotationsRepository, int $id, PaginatorInterface $paginator, Request $request): Response
    {
        $films  =$paginator->paginate(
            $films= $repository->findOneByIdJoinedToCotation($id),
            $request->query->getInt('page',1),
            12
        );
        $filmsCotations = $cotationsRepository->find($id);
        $cotations = $cotationsRepository->findAll();
        return $this->render('pages/list.html.twig', [
            'films' => $films,
            'cotations' => $cotations,
            'filmsCotations' => $filmsCotations
        ]);
    }


    /*Affiche la liste des films en fonction de leurs titre*/
    /**
     * @Route("/listtitre", name="listtitre")
     * @param Request $request
     * @param FilmRepository $filmRepository
     * @return Response
     */
    public function affichetitre( Request $request, FilmRepository $filmRepository){

        $search = new Film();
        $form = $this->createForm(FilmType::class, $search);
        $form->handleRequest($request);
        $motcle = $request->get('search');
        $films = $filmRepository->findByTitle($motcle);
        return $this->render('pages/categorie.html.twig', array(
            'search' => $search,
            'films' => $films
        ));
    }

}