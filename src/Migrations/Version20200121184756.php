<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200121184756 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE film DROP FOREIGN KEY FK_8244BE22270C6BF7');
        $this->addSql('DROP INDEX IDX_8244BE22270C6BF7 ON film');
        $this->addSql('ALTER TABLE film ADD cotation INT DEFAULT NULL, CHANGE cotation_id cotation_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE film ADD CONSTRAINT FK_8244BE22270C6BF7 FOREIGN KEY (cotation_id_id) REFERENCES cotation (id)');
        $this->addSql('CREATE INDEX IDX_8244BE22270C6BF7 ON film (cotation_id_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE film DROP FOREIGN KEY FK_8244BE22270C6BF7');
        $this->addSql('DROP INDEX IDX_8244BE22270C6BF7 ON film');
        $this->addSql('ALTER TABLE film ADD cotation_id INT DEFAULT NULL, DROP cotation_id_id, DROP cotation');
        $this->addSql('ALTER TABLE film ADD CONSTRAINT FK_8244BE22270C6BF7 FOREIGN KEY (cotation_id) REFERENCES cotation (id)');
        $this->addSql('CREATE INDEX IDX_8244BE22270C6BF7 ON film (cotation_id)');
    }
}
